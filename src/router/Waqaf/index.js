import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'

const waqafList = [
  { id: '1', symbol: 'م', explanation: 'Waqaf Lazim - Harus berhenti.' },
  { id: '2', symbol: 'ط', explanation: 'Waqaf Mutlaq - Disarankan berhenti.' },
  { id: '3', symbol: 'ج', explanation: 'Waqaf Jibril - Boleh berhenti atau lanjut.' },
  { id: '4', symbol: 'ز', explanation: 'Waqaf Jaiz - Disarankan untuk tidak berhenti.' },
  { id: '5', symbol: 'ص', explanation: 'Waqaf Murakhas - Boleh berhenti di keadaan darurat.' },
  { id: '6', symbol: 'ق', explanation: 'Qila Wakaf - Ada perbedaan pendapat boleh berhenti atau lanjut.' },
  { id: '7', symbol: 'ل', explanation: 'Waqaf Laa - Tidak boleh berhenti.' },
  { id: '8', symbol: 'قف', explanation: 'Waqaf - Berhenti.' },
  { id: '9', symbol: 'س', explanation: 'Saktah - Berhenti sejenak tanpa mengambil nafas.' },
  { id: '10', symbol: '∴', explanation: 'Waqaf - Teruskan bacaan.' },
];
const Waqaf = () => {

  return (
    <View style={styles.container}>     
      <FlatList
      data={waqafList}
      renderItem={({item}) => (
      <View style={styles.item}> 
        <Text style={styles.huruf}>{item.symbol}</Text>
        <Text style={styles.nama}>{item.explanation}</Text>
      </View>
      )}
      />
    </View>
  );
}

export default Waqaf

const styles = StyleSheet.create({
  container :{
    backgroundColor : '#E0E0D4',
    flex : 1,
    alignItems : 'center',
    justifyContent : 'center',
    padding : 10,
  },

  item : {
    backgroundColor : '#E0E0D4',
    padding : 10,
    marginVertical : 8,
    borderWidth : 1,
    borderColor : '#007667',
    borderRadius : 10,},
  huruf : {
    fontSize : 20,
    fontWeight : 'bold',
    color : '#000',

  },
  nama : {
    fontSize : 17,
    fontWeight : 'bold',
    color : '#000',

  },
  
})