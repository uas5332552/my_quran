import { StyleSheet, Text, View,SafeAreaView,ScrollView, Linking } from 'react-native'
import React from 'react'

const PrivasiPolice = () => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <Text style={styles.title}>Kebijakan Privasi Aplikasi Al-Quran</Text>
        <Text style={styles.date}>Tanggal Efektif: 4 Juli 2024</Text>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>1. Pendahuluan</Text>
          <Text style={styles.text}>
            Selamat datang di aplikasi Al-Quran. Kami menghargai privasi Anda dan berkomitmen untuk melindungi data pribadi Anda. Kebijakan privasi ini menjelaskan bagaimana kami mengumpulkan, menggunakan, dan melindungi informasi Anda ketika Anda menggunakan aplikasi kami.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>2. Informasi yang Kami Kumpulkan</Text>
          <Text style={styles.text}>
            Kami mengumpulkan informasi berikut:
            {'\n'}- Informasi Non-Pribadi: Kami dapat mengumpulkan informasi non-pribadi yang tidak mengidentifikasi pengguna secara individual, seperti data penggunaan aplikasi, jenis perangkat, dan sistem operasi.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>3. Penggunaan Informasi</Text>
          <Text style={styles.text}>
            Informasi yang kami kumpulkan digunakan untuk:
            {'\n'}- Meningkatkan pengalaman pengguna.
            {'\n'}- Memahami bagaimana aplikasi digunakan dan melakukan peningkatan kinerja.
            {'\n'}- Memberikan pembaruan dan pemberitahuan terkait aplikasi.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>4. Keamanan</Text>
          <Text style={styles.text}>
            Kami berkomitmen untuk menjaga keamanan informasi Anda dan melindungi dari akses tidak sah, penggunaan, atau pengungkapan. Namun, kami ingin mengingatkan pengguna bahwa tidak ada metode transmisi data melalui internet atau metode penyimpanan elektronik yang 100% aman.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>5. Pembagian Informasi</Text>
          <Text style={styles.text}>
            Kami tidak membagikan informasi pribadi Anda dengan pihak ketiga kecuali jika diperlukan untuk:
            {'\n'}- Mematuhi hukum atau proses hukum yang berlaku.
            {'\n'}- Menegakkan persyaratan layanan kami.
            {'\n'}- Melindungi hak, privasi, keamanan, atau properti kami dan pengguna kami.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>6. Perubahan Kebijakan Privasi</Text>
          <Text style={styles.text}>
            Kami dapat memperbarui kebijakan privasi ini dari waktu ke waktu. Perubahan akan segera berlaku setelah kebijakan privasi yang direvisi diposting dalam aplikasi. Kami mendorong pengguna untuk secara berkala meninjau kebijakan privasi ini agar tetap mengetahui bagaimana kami melindungi informasi Anda.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>7. Kontak Kami</Text>
          <Text style={styles.text}>
            Jika Anda memiliki pertanyaan atau kekhawatiran tentang kebijakan privasi ini atau praktik privasi kami, silakan hubungi kami di:
          </Text>
          <Text onPress={() => Linking.openURL('mailto:wmi54968@gmail.com')} style={styles.link}>AlQuran.com</Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>8. Persetujuan Anda</Text>
          <Text style={styles.text}>
            Dengan menggunakan aplikasi kami, Anda menyetujui kebijakan privasi ini.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>9. Hak Anda</Text>
          <Text style={styles.text}>
            Meskipun kami tidak mengumpulkan informasi pribadi yang dapat diidentifikasi, Anda memiliki hak untuk meminta informasi tentang data apa pun yang kami miliki tentang Anda. Namun, karena kami tidak mengumpulkan informasi pribadi, hak ini tidak berlaku dalam konteks aplikasi kami.
          </Text>
        </View>
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>10. Aplikasi Pihak Ketiga</Text>
          <Text style={styles.text}>
            Aplikasi kami dapat berisi tautan ke situs web atau aplikasi pihak ketiga. Kami tidak bertanggung jawab atas kebijakan privasi atau konten dari situs web atau aplikasi pihak ketiga tersebut. Kami mendorong Anda untuk membaca kebijakan privasi dari setiap situs web atau aplikasi yang Anda kunjungi.
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default PrivasiPolice

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10,
    
  },
  contentContainer: {
    padding: 20,
},
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'black',
  },
  date: {
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 20,
    color: '#666',
  },
  section: {
    marginBottom: 20,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    color : 'black',
  },
  text: {
    fontSize: 16,
    color: '#333',
    textAlign : 'justify',
  },
  link: {
    fontSize: 16,
    color: 'blue',
    textAlign : 'justify',
    
  },
});
