import { StyleSheet, Text, View, SafeAreaView, ScrollView, Image } from 'react-native'
import React from 'react'

const InfoQuran = () => {
  return (
    <SafeAreaView style={styles.container}>
    <ScrollView contentContainerStyle={styles.scrollView}>
      <View style={styles.header}>
        <Image
          source={require('../../assets/icon/logo1.png')} 
          style={styles.logo}
        />
        <Text style={styles.title}>Aplikasi Al-Qur'an Info</Text>
      </View>

      <View style={styles.section}>
        <Text style={styles.sectionTitle}>Apa itu Al-Qur'an?</Text>
        <Text style={styles.text}>
          Al-Qur'an adalah kitab suci umat Islam yang dianggap sebagai wahyu terakhir dari Allah SWT
          kepada Nabi Muhammad SAW. Al-Qur'an diturunkan dalam bahasa Arab dan berisi petunjuk
          untuk kehidupan manusia.
        </Text>
      </View>

      <View style={styles.section}>
        <Text style={styles.sectionTitle}>Manfaat Membaca Al-Qur'an</Text>
        <Text style={styles.text}>
          Membaca Al-Qur'an memiliki banyak manfaat, antara lain mendapatkan pahala besar,
          mendapatkan petunjuk hidup, meningkatkan keimanan, dan sebagainya.
        </Text>
      </View>

      <View style={styles.section}>
        <Text style={styles.sectionTitle}>Cara Menggunakan Aplikasi Ini</Text>
        <Text style={styles.text}>
          Aplikasi ini menyediakan informasi umum tentang Al-Qur'an. Anda dapat langsung menggunakan Aplikasi ini
          untuk membacanya, dan mengatahui manfaatnya.
        </Text>
      </View>
    </ScrollView>
  </SafeAreaView>
);
}

export default InfoQuran

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : '#E0E0D4',
    
  },
  scrollView: {
    flexGrow : 1,
    paddingVertical:20,
    paddingHorizontal :16,
  },
  header: {
  alignItems : 'center',
  marginBottom : 20,
  },
  logo: {
    width :100,
    height :100,
    resizeMode :'contain',
  },
  title:{
    fontSize:24,
    fontWeight: 'bold',
    marginTop :10,
    color : '#000',
  },
  section : {
  marginBottom :20,
  },
  sectionTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    color : '#000',
  },
  text: {
    fontSize: 16,
    lineHeight: 24,
    color : '#000',
  },
})