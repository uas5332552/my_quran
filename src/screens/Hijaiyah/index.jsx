import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'

const hijaiyah = [
  { key: 'أ', name: 'Alif', description: 'Tidak memiliki harakat sendiri, berfungsi sebagai huruf mati atau panjang (mad).' },
  { key: 'ب', name: 'Ba', description: 'Berbunyi seperti \'b\' dalam bahasa Indonesia.' },
  { key: 'ت', name: 'Ta', description: 'Berbunyi seperti \'t\' dalam bahasa Indonesia.' },
  { key: 'ث', name: 'Tsa', description: 'Berbunyi seperti \'th\' dalam bahasa Inggris (thin).' },
  { key: 'ج', name: 'Jim', description: 'Berbunyi seperti \'j\' dalam bahasa Indonesia.' },
  { key: 'ح', name: 'Ha', description: 'Bunyi \'h\' yang berat, lebih keras dari \'h\' biasa.' },
  { key: 'خ', name: 'Kha', description: 'Berbunyi seperti \'kh\' dalam kata \'akhir\'.' },
  { key: 'د', name: 'Dal', description: 'Berbunyi seperti \'d\' dalam bahasa Indonesia.' },
  { key: 'ذ', name: 'Dzal', description: 'Berbunyi seperti \'th\' dalam bahasa Inggris (that).' },
  { key: 'ر', name: 'Ra', description: 'Berbunyi seperti \'r\' yang bergetar.' },
  { key: 'ز', name: 'Zai', description: 'Berbunyi seperti \'z\' dalam bahasa Indonesia.' },
  { key: 'س', name: 'Sin', description: 'Berbunyi seperti \'s\' dalam bahasa Indonesia.' },
  { key: 'ش', name: 'Syin', description: 'Berbunyi seperti \'sh\' dalam bahasa Inggris (shine).' },
  { key: 'ص', name: 'Shad', description: 'Bunyi \'s\' yang lebih tebal dan berat.' },
  { key: 'ض', name: 'Dhad', description: 'Bunyi \'d\' yang lebih tebal dan berat.' },
  { key: 'ط', name: 'Tha', description: 'Bunyi \'t\' yang lebih tebal dan berat.' },
  { key: 'ظ', name: 'Zha', description: 'Bunyi \'z\' yang lebih tebal dan berat.' },
  { key: 'ع', name: 'Ain', description: 'Bunyi yang unik, agak seperti menelan ludah.' },
  { key: 'غ', name: 'Ghain', description: 'Berbunyi seperti \'gh\' dalam kata Prancis \'rendezvous\'.' },
  { key: 'ف', name: 'Fa', description: 'Berbunyi seperti \'f\' dalam bahasa Indonesia.' },
  { key: 'ق', name: 'Qaf', description: 'Bunyi \'k\' yang lebih dalam.' },
  { key: 'ك', name: 'Kaf', description: 'Berbunyi seperti \'k\' dalam bahasa Indonesia.' },
  { key: 'ل', name: 'Lam', description: 'Berbunyi seperti \'l\' dalam bahasa Indonesia.' },
  { key: 'م', name: 'Mim', description: 'Berbunyi seperti \'m\' dalam bahasa Indonesia.' },
  { key: 'ن', name: 'Nun', description: 'Berbunyi seperti \'n\' dalam bahasa Indonesia.' },
  { key: 'ه', name: 'Ha', description: 'Berbunyi seperti \'h\' dalam bahasa Indonesia.' },
  { key: 'و', name: 'Waw', description: 'Berbunyi seperti \'w\' dalam bahasa Indonesia atau sebagai huruf mad (panjang).' },
  { key: 'ي', name: 'Ya', description: 'Berbunyi seperti \'y\' dalam bahasa Indonesia atau sebagai huruf mad (panjang).' },
  { key: 'ء', name: 'Hamzah', description: 'Berfungsi sebagai pemisah antara dua vokal, seperti \'u\' dalam kata \'uh-oh\'.' },
];
const Hijaiyah = () => {

  return (
    <View style={styles.container}>     
      <FlatList
      data={hijaiyah}
      renderItem={({item}) => (
      <View style={styles.item}> 
        <Text style={styles.huruf}>{item.key}</Text>
        <Text style={styles.nama}>{item.name}</Text>
        <Text style={styles.deskripsi}>{item.description}</Text>
      </View>
      )}
      />
    </View>
  );
}

export default Hijaiyah

const styles = StyleSheet.create({
  container :{
    backgroundColor : '#E0E0D4',
    flex : 1,
    alignItems : 'center',
    justifyContent : 'center',
    padding : 10,
  },

  item : {
    backgroundColor : '#007667',
    padding : 10,
    marginVertical : 8,
    borderRadius : 10,},
  huruf : {
    fontSize : 32,
    fontWeight : 'bold',
    color : '#fff',

  },
  nama : {
    fontSize : 24,
    fontWeight : 'bold',
    color : '#fff',

  },
  deskripsi : {
    fontSize : 16,
    color : '#fff',
  }
})