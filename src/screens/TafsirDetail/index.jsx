import {StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';

const Detail = ({route}) => {
    const {TafsirNumber} = route.params;
    const [dataTafsir, setDataTafsir] = useState([]);
    const [font, setFont] = useState(15);
  //fungsi untuk get data
  const getData = async () => {
    try {
      const respon = await fetch(`https://equran.id/api/v2/tafsir/${TafsirNumber}`);
      const data = await respon.json();
      return setDataTafsir(data.data.tafsir);
      
      
    } catch (error) {
      console.log(error);
    }
    
  };

  useEffect(() => {
    getData();
  });

  const tambahFont = () => {
    setFont(font + 1);
  };
  const kurangFont = () => {
    setFont(font - 1);
  };

  return (
    <ScrollView style={{backgroundColor : '#E0E0D4',
    }}>
      
      <View style={{flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <TouchableOpacity onPress={() => tambahFont()} style={styles.btn}>
              <Text style={styles.plus }>+</Text>
              </TouchableOpacity> 
            </View>
            <View>
              <TouchableOpacity onPress={() => kurangFont()} style={styles.btn}>
              <Text style={styles.plus }>-</Text>
              </TouchableOpacity> 
            </View>
            </View>
          </View>
      {dataTafsir &&
        dataTafsir.map((item, i) => {
          return (
            <View style={{display: 'flex', flexDirection: 'column' ,width: '100%', borderWidth: 1,
                borderColor: '#007667',
                padding: 5, alignItems:'center'}} key={i}>
              <View style={{flex: 1, borderWidth:2,borderColor:'#007667',borderRadius:50, width:50, height:40, justifyContent:'center'}}>
                <Text style={styles.text1}>{item.ayat} </Text>
                </View>
              
              <View style={{flex: 1}}>
                <Text style={{fontSize:font, color:'black',paddingBottom : 10, textAlign : 'justify', paddingTop:10}}>{item.teks}</Text>
              </View>
             
            </View>
         
           
          );
        })}
      
    </ScrollView>
  );
};

export default Detail;

const styles = StyleSheet.create({
    text1: {
        fontWeight: 'bold',
        textAlign: 'center',

        paddingBottom : 5,
        color: 'black',
        fontSize: 20,
      },
    text: {
        
        color: 'black',
        fontSize: 20,
    },
        btn : {
            backgroundColor :'#007667',
            width: 60,
            height: 30,
            margin : 10,
            borderRadius : 5,
            
        },
        plus : {
          color : 'white',
          textAlign : 'center',
          fontSize : 25,
        }
});
