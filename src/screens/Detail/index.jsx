import {StyleSheet, Text, View, ScrollView, TouchableOpacity, ActivityIndicator} from 'react-native';
import React, {useEffect, useState} from 'react';

const Detail = ({route}) => {
    const {surahNumber} = route.params;
    const [dataQuran, setDataQuran] = useState([]);
    const [font, setFont] = useState(25);
    const [loading, setLoading]=useState(true);
    const [isMode, setMode]=useState(false);
  
  const getData = async () => {
    try {
      const respon = await fetch(`https://equran.id/api/v2/surat/${surahNumber}`);
      const dataAyah = await respon.json();
      return setDataQuran(dataAyah.data.ayat);
      
      
    } catch (error) {
      console.log(error);
    }finally {
      setLoading(false);}
    
  };

  useEffect(() => {
    getData();
  });

  const tambahFont = () => {
    setFont(font + 1);
  };
  const kurangFont = () => {
    setFont(font - 1);
  };
  const toggelMode = () => {
    setMode(!isMode)
  };
  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator color='#007667'/>
      </View>
    )
  }
  

  return (
    <ScrollView style={[styles.container, isMode ? styles.darkContainer : styles.lightContainer]}>
      
      <View style={styles.fontControlContainer}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <TouchableOpacity onPress={() => tambahFont()} style={styles.btn}>
              <Text style={styles.plus }>+</Text>
              </TouchableOpacity> 
            </View>
            <View>
              <TouchableOpacity onPress={() => kurangFont()} style={styles.btn}>
              <Text style={styles.plus }>-</Text>
              </TouchableOpacity> 
            </View>
            <View>
              <TouchableOpacity onPress={() => toggelMode()} style={styles.btn}>
              <Text style={styles.mode }>{isMode ? 'Light' : 'Dark'}</Text>
              </TouchableOpacity> 
            </View>
            </View>
          </View>
      {dataQuran &&
        dataQuran.map((item, i) => {
          return (
            <View style={styles.jarak} key={i}>
               
                  <Text style={{fontSize:font, color:'black', color : isMode ? 'white' : 'black'}} >{item.teksArab}</Text>
                  <Text style={[styles.text, isMode ? styles.darkText : styles.lightText]} >{item.teksIndonesia}({item.nomorAyat})</Text>
                  </View>
          );
        })}
      
    </ScrollView>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    backgroundColor : '#E0E0D4',
    width: '100%',
    borderWidth: 1,
    borderColor: '#007667',
    padding: 5,
  },
  darkContainer : {
    backgroundColor : '#333333',
  },
  lightContainer : {
    backgroundColor : '#E0E0D4',
  },
  jarak:{
    padding: 7,
  },
  text :{
    fontSize : 15,
    color: 'black',
  },
  darkText: {
      color: 'white',
  },
  lightText: {
      color: 'black',
  },
  btn : {
    backgroundColor :'#007667',
    width: 60,
    height: 30,
    margin : 7,
    borderRadius : 5,
    paddingBottom: 4,
    
},
plus : {
  color : 'white',
  textAlign : 'center',
  fontSize : 25,
},
mode : {
  color : 'white',
  paddingTop : 5,
  textAlign : 'center',
  fontSize : 15,
},
fontControlContainer: {
  flexDirection: 'row',
  justifyContent: 'center',
  marginBottom: 10,
},
loader: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
},
errorText: {
  color: 'red',
  fontSize: 18,
},

});
