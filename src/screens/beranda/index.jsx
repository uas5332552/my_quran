import {StyleSheet, View, Image, TouchableOpacity, Text} from 'react-native';
import React from 'react';

const AlQuran = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#E0E0D4', height:'100%'}}>
      <View>
        <Image style={styles.img1} source={require('../../assets/img/bis.jpg')} />
      </View>
      <View></View>
      <View>
        <Image style={styles.img2} source={require('../../assets/img/1.png')} />
      </View>
      <View>
      
      <View style={styles.menu}>
        <View style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Hijaiyah')}
            style={styles.icon}>
            <Image source={require('../../assets/img/3.png')} style={styles.img} />
            <Text style={styles.txt}>HIJAIYAH</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity style={styles.icon}
          onPress={() => navigation.navigate('Surah')}>
            <Image source={require('../../assets/img/4.png')} style={styles.img} />
            <Text style={styles.txt}>SURAH</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('TafsirQuran')}
          style={styles.icon}>
            <Image source={require('../../assets/img/6.png')} style={styles.img} />
            <Text style={styles.txt}>TAFSIR QURAN</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.menu}>
        <View style={{flex: 1}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('Tajwid')}
          style={styles.icon}>
            <Image source={require('../../assets/img/5.png')} style={styles.img} />
            <Text style={styles.txt}>Tajwid</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('Doa')}
          style={styles.icon}>
            <Image source={require('../../assets/img/7.png')} style={styles.img} />
            <Text style={styles.txt}>DOA-DOA</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity
          onPress={() => navigation.navigate('LayananLain')}
          style={styles.icon}>
            <Image source={require('../../assets/img/m.jpg')} style={{width: 70,
    height: 70,
    justifyContent: 'center',
    margin: '0 auto',}} />
            <Text style={styles.txt}>Layanan Lain</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
      </View>
    
  );
};

export default AlQuran;

const styles = StyleSheet.create({
  img1: {
    width: '100%',
    height: 100,
  },
  img2: {
    width: '100%',
    height: 360,
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
  },
  img: {
    width: 100,
    height: 70,
    justifyContent: 'center',
    margin: '0 auto',
  },
  icon: {
    margin: 5,
    padding: 1,

    backgroundColor: 'white',
    width: 110,
    height: 100,
    alignItems: 'center',
    borderRadius: 15,
  },
  txt: {
    color: 'black',
    padding: 2,
  },
});
