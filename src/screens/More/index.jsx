import {StyleSheet, View, Image, TouchableOpacity, Text} from 'react-native';
import React from 'react';

const More = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#E0E0D4'}}>
      <View style={styles.menu}>
        <View style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('InfoQuran')}
            style={styles.icon}>
            <Image source={require('../../assets/img/i.jpg')} style={styles.img} />
            <Text style={styles.txt}>Info Quran</Text>
          </TouchableOpacity>
        </View>
      
        <View style={{flex: 1}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('Waqaf')}
          style={styles.icon}>
            <Image source={require('../../assets/img/w.jpg')} style={styles.img} />
            <Text style={styles.txt}>Tanda Waqof</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('PrivasiPolice')}
          style={styles.icon}>
            <Image source={require('../../assets/img/prfs.jpg')} style={styles.img} />
            <Text style={styles.txt}>Kebijakan Privasi</Text>
          </TouchableOpacity>
        </View>
      </View>
    
      </View>
    
  );
};

export default More;

const styles = StyleSheet.create({
  
  menu: {
    paddingTop: 5,
    display: 'flex',
    flexDirection: 'row',
  },
  img: {
    width: 100,
    height: 70,
    justifyContent: 'center',
    margin: '0 auto',
  },
  icon: {
    margin: 5,
    padding: 1,

    backgroundColor: 'white',
    width: 110,
    height: 100,
    alignItems: 'center',
    borderRadius: 15,
  },
  txt: {
    color: 'black',
    padding: 2,
  },
});
