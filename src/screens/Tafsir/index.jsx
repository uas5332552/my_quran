import {
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {useEffect, useState} from 'react';
  
  const Tafsir = ({navigation}) => {
    const apiUrl = 'https://equran.id/api/v2/surat';
    const [data, setData] = useState([]);
    //fungsi untuk get data
    const getData = async () => {
      try {
        const respon = await fetch(apiUrl);
        const dataJson = await respon.json();
        return setData(dataJson.data);
      } catch (error) {
        console.log(error);
      }
    };
  
    useEffect(() => {
      getData();
    }, []);
  
    return (
      <ScrollView>
        {data &&
          data.map((item, i) => {
            return (
              <TouchableOpacity
                key={i}
                style={styles.tombol}
                onPress={() =>
                  navigation.navigate('TafsirDetail',{TafsirNumber:item.nomor})
                }>
                <View style={{display: 'flex', flexDirection: 'row'}}>
                  <View style={{flex: 1}}>
                    <Text style={styles.text}>{item.nomor}. </Text>
                  </View>
  
                  <View style={{flex: 1}}>
                    <Text style={styles.text}>{item.namaLatin}</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text style={styles.text}>{item.nama}</Text>
                  </View>
  
                </View>
                <View style={{display: 'flex', flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                      <Text style={styles.text2} >Tempat Turun : {item.tempatTurun}, Jumlah Ayat : {item.jumlahAyat}</Text>
                    </View>
                 
                </View>
              </TouchableOpacity>
            );
          })}
      </ScrollView>
    );
  };
  
  export default Tafsir;
  
  const styles = StyleSheet.create({
    tombol: {
      width: '100%',
      borderWidth: 1,
      borderColor: '#007667',
      padding: 5,
    },
    text: {
      flex: 1,
      color: 'black',
      fontSize: 20,
    },
    text2: {
      flex: 1,
      color: 'black',
      fontSize: 10,
      textAlign : 'center',
    },
  });
  