import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'

const data = [
  { key: 'أ', name: 'Alif', tajwid: 'Mad', description: 'Mad adalah perpanjangan suara pada huruf Alif, Waw, dan Ya.' },
  { key: 'ب', name: 'Ba', tajwid: 'Qalqalah', description: 'Qalqalah adalah suara pantulan pada huruf Ba, Jim, Dal, Tha, dan Qaf.' },
  { key: 'ت', name: 'Ta', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ث', name: 'Tsa', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ج', name: 'Jim', tajwid: 'Qalqalah', description: 'Qalqalah adalah suara pantulan pada huruf Ba, Jim, Dal, Tha, dan Qaf.' },
  { key: 'ح', name: 'Ha', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'خ', name: 'Kha', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'د', name: 'Dal', tajwid: 'Qalqalah', description: 'Qalqalah adalah suara pantulan pada huruf Ba, Jim, Dal, Tha, dan Qaf.' },
  { key: 'ذ', name: 'Dzal', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ر', name: 'Ra', tajwid: 'Idgham', description: 'Idgham adalah memasukkan suara nun mati atau tanwin ke dalam huruf berikutnya.' },
  { key: 'ز', name: 'Zai', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'س', name: 'Sin', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ش', name: 'Syin', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ص', name: 'Shad', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ض', name: 'Dhad', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ط', name: 'Tha', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ظ', name: 'Zha', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ع', name: 'Ain', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'غ', name: 'Ghain', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ف', name: 'Fa', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ق', name: 'Qaf', tajwid: 'Qalqalah', description: 'Qalqalah adalah suara pantulan pada huruf Ba, Jim, Dal, Tha, dan Qaf.' },
  { key: 'ك', name: 'Kaf', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ل', name: 'Lam', tajwid: 'Idgham', description: 'Idgham adalah memasukkan suara nun mati atau tanwin ke dalam huruf berikutnya.' },
  { key: 'م', name: 'Mim', tajwid: 'Idgham', description: 'Idgham adalah memasukkan suara nun mati atau tanwin ke dalam huruf berikutnya.' },
  { key: 'ن', name: 'Nun', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'ه', name: 'Ha', tajwid: 'Ikhfa', description: 'Ikhfa adalah menyembunyikan suara nun mati atau tanwin dengan suara dengung.' },
  { key: 'و', name: 'Waw', tajwid: 'Mad', description: 'Mad adalah perpanjangan suara pada huruf Alif, Waw, dan Ya.' },
  { key: 'ي', name: 'Ya', tajwid: 'Mad', description: 'Mad adalah perpanjangan suara pada huruf Alif, Waw, dan Ya.' },
  { key: 'ء', name: 'Hamzah', tajwid: 'Wasl', description: 'Wasl adalah pengucapan Hamzah hanya ketika di awal kata.' },
];

const Tajwid = () => {

  return (
    <View style={styles.container}>     
      <FlatList
      data={data}
      renderItem={({item}) => (
      <View style={styles.item}> 
        <Text style={styles.huruf}>{item.key}</Text>
        <Text style={styles.nama}>{item.name}</Text>
        <Text style={styles.nama}>{item.tajwid}</Text>
        <Text style={styles.deskripsi}>{item.description}</Text>
      </View>
      )}
      />
    </View>
  );
}

export default Tajwid

const styles = StyleSheet.create({
  container :{
    flex : 1,
    alignItems : 'center',
    justifyContent : 'center',
    padding : 10,
    backgroundColor : '#E0E0D4',
    
  },

  item : {
    backgroundColor : '#007667',
    padding : 10,
    marginVertical : 8,
    borderRadius : 10,},
  huruf : {
    fontSize : 32,
    fontWeight : 'bold',
    color : '#fff',

  },
  nama : {
    fontSize : 24,
    fontWeight : 'bold',
    color : '#fff',

  },
  deskripsi : {
    fontSize : 16,
    color : '#fff',
  }
})