import {StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';

const Doa = () => {
    const apiUrl = 'https://api.dikiotang.com/doa/harian';
    const [data, setData] = useState([]);
    const [font, setFont] = useState(20);
    //fungsi untuk get data
    const getData = async () => {
      try {
        const respon = await fetch(apiUrl);
        const dataJson = await respon.json();
        return setData(dataJson.data);
      } catch (error) {
        console.log(error);
      }
    };
  
    useEffect(() => {
      getData();
    }, []);
    
    const tambahFont = () => {
      setFont(font + 1);
    };
    const kurangFont = () => {
      setFont(font - 1);
    };
    return (
      <ScrollView style={{backgroundColor : '#E0E0D4',
      }}>
        <View style={{flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <TouchableOpacity onPress={() => tambahFont()} style={styles.btnPlus}>
              <Text style={styles.plus }>+</Text>
              </TouchableOpacity> 
            </View>
            <View>
              <TouchableOpacity onPress={() => kurangFont()} style={styles.btnPlus}>
              <Text style={styles.plus }>-</Text>
              </TouchableOpacity> 
            </View>
            </View>
          </View>
        {data &&
          data.map((item, i) => {
            return (
               <View style={{display: 'flex', flexDirection: 'column' ,width: '100%',
                    borderWidth: 1,
                    borderColor: '#007667',
                    padding: 5}} key={i}>
                  
                  <View style={{flex: 1}}>
                    <Text style={styles.text1}>{item.judul} </Text>
                    </View>
                  
                  <View style={{flex: 1}}>
                    <Text style={{fontSize:font, color:'black',paddingBottom : 10}}>{item.arab}</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text style={{fontSize:font, color:'black',paddingBottom : 10}}>{item.indo}</Text>
                  </View>
                </View>
             
            );
          })}
      </ScrollView>
    );
  };
  
export default Doa

const styles = StyleSheet.create({
    text1: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom : 10,
        marginBottom : 10,
        color: 'black',
        fontSize: 20,
      },
      text: {
        
        color: 'black',
        fontSize: 20,
      },
      btnPlus : {
        backgroundColor :'#007667',
        width: 60,
        height: 30,
        margin : 10,
        borderRadius : 5,
        
    },
    plus : {
      color : 'white',
      textAlign : 'center',
      fontSize : 25,
    }

})