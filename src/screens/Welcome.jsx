import { StyleSheet, Text, View , TouchableOpacity, Image} from 'react-native'
import React from 'react'

const Welcome = ({navigation}) => {
  return (
    <View style={styles.baground} >
        <View style={{flex:1}}>
         <Text style={styles.text1}>Welcome To My AlQuran</Text>
  
        </View>
        <View style={{flex:1}}>
        <Image style={styles.img1} source={require('../assets/img/mas.jpg')} />
      </View>
        <View style={{flex: 1}}>
          <TouchableOpacity style={styles.icon}
          onPress={() => navigation.navigate('ALQuran')}>
            <Text style={styles.text2}>Get Start</Text>
          </TouchableOpacity>
        </View>
      

    </View>
  )
}

export default Welcome

const styles = StyleSheet.create({
  baground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#007667',
    padding : 20,
  
  },

  text1: {
    fontSize: 35,
    marginTop : 20,
    paddingTop : 20,
    color : '#fff',
    textAlign : 'center',

  },
  img1: {
    width : 400,
    height : 205,
  },
  text2: {
    fontSize: 20,
    color: '#007667',
    textAlign : 'center',
    paddingTop : 11,
    
  },
  icon: { 
    backgroundColor: '#fff',
    width : 200,
    height : 50,
    borderRadius : 10,

    

  }
})