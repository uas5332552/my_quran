import Surah from './screens/surah';
import AlQuran from './screens/beranda';
import More from './screens/More';
import Detail from './screens/Detail';
import Doa from './screens/Doa';
import Hijaiyah from './screens/Hijaiyah';
import Tajwid from './screens/Tajwid';
import InfoQuran from './router/InfoQuran';
import Waqaf from './router/Waqaf';
import PrivasiPolice from './router/PrivasiPolice';
import Tafsir from './screens/Tafsir';
import TafsirDetail from './screens/TafsirDetail';
export {Surah, AlQuran, More, Detail, Doa, Hijaiyah,  Tajwid, InfoQuran, Waqaf, TafsirDetail, Tafsir, PrivasiPolice};

