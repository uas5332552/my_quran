import {StyleSheet, View, Image} from 'react-native';
import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {AlQuran, Detail, Hijaiyah, Doa,  Surah, More, InfoQuran,  PrivasiPolice, TafsirDetail, Tafsir, Waqaf, Tajwid} from '.';
import SplashScreen from 'react-native-splash-screen';
import Welcome from './screens/Welcome';

const SplashScreenComponent = ({ navigation }) => {
  useEffect(() => {
    SplashScreen.hide(); 
    setTimeout(() => {
      navigation.replace('Welcome'); 
    }, 2000); 
  }, [navigation]);
  return (
    <View style={styles.splashContainer}>
      <Image source={require('../src/assets/icon/logo1.png')}/>
    </View>
  );
};

const Stack = createNativeStackNavigator();
const StackNav = () => {
  return (
    
      <Stack.Navigator initialRouteName='SplashScreen'
      screenOptions={{
        headerStyle: {
          backgroundColor: '#007667',
        },
        headerTintColor: '#fff',
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontweight: 'bold',
        },}}>
        <Stack.Screen options={{headerShown: false}} name="SplashScreen" component={SplashScreenComponent} />
        <Stack.Screen options={{headerShown: false}} name="Welcome" component={Welcome} />
        <Stack.Screen options={{headerShown: false}}  name="ALQuran" component={AlQuran} />
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="Surah" component={Surah} />
        <Stack.Screen name="LayananLain" component={More} />
        <Stack.Screen name="Doa" component={Doa} />
        <Stack.Screen name="Hijaiyah" component={Hijaiyah} />
        <Stack.Screen name="Tajwid" component={Tajwid} />

        <Stack.Screen  name='AlQuran' component={StackNav}/>
        <Stack.Screen  name="TafsirDetail" component={TafsirDetail} />
        <Stack.Screen  name="InfoQuran" component={InfoQuran} />
        <Stack.Screen  name="PrivasiPolice" component={PrivasiPolice} />
        <Stack.Screen  name="TafsirQuran" component={Tafsir} />
        <Stack.Screen  name="Waqaf" component={Waqaf} />
        
      </Stack.Navigator>
    
  )}

function App () {

  return(
  <NavigationContainer>
    <StackNav/>
  </NavigationContainer>
  );
}

export default App;

const styles = StyleSheet.create({
  splashContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#007667',
  },
  
});